package MonteCarloSimulation;

import java.util.List;
import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class AsianCallOption implements Payout {

	private double K;

	public AsianCallOption(double K){
		this.K = K;
	}

	public double getPayout(StockPath path) {
		List<Pair<DateTime, Double>> prices = path.getPrices();
		double sum = 0.0;
		for (int i = 0; i < prices.size(); i++){
			sum += prices.get(i).getValue();
		}
		double mean = sum/prices.size();
		return Math.max(0, mean - K);
	}
}