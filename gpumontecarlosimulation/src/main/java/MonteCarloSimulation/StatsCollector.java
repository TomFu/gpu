package MonteCarloSimulation;

public class StatsCollector {

	private double _mean;
	private double _var;
	private double _MS;
	private int _size;

	public StatsCollector(){
		this._mean = 0;
		this._var = 0;
		this._MS = 0;
		this._size = 0;
	}

	public void update(double price_new){
		_mean = _mean*_size/(_size+1)+price_new/(_size+1);
		_MS = _MS*_size/(_size+1)+price_new*price_new/(_size+1);
		_var = _MS - _mean*_mean;
		_size++;
	}

	public boolean withinIntvl(double err, double y){
		return y*Math.sqrt(_var/_size)<err;
	}

	public int getNum(){
		return _size;
	}

	public double getMean(){
		return _mean;
	}

	public double getMS(){
		return _MS;
	}

	public double getVar(){
		return _var;
	}
}
