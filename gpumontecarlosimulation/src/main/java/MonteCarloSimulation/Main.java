package MonteCarloSimulation;

import org.joda.time.DateTime;
import org.apache.commons.math3.distribution.NormalDistribution;
/*
* Simulation manager: main function
* */
public class Main {
	// main function
	public static void main(String[] args){
		//Input data:
		int N = 252;
		double rate = 0.0001;
		double S0 = 152.35;
		double vol = 0.01;
		double err = 0.01;
		double K_Euro = 165;
		double K_Asian = 164;
		double confidencelvl=0.96;
		int batch = 1024*1024;

		NormalDistribution stdNormal = new NormalDistribution();
		double y = stdNormal.inverseCumulativeProbability(confidencelvl+(1-confidencelvl)/2);
		DateTime startDate = new DateTime();
		DateTime endDate = new DateTime();

		GaussianRandomNumberGenerator_GPU gpu = new GaussianRandomNumberGenerator_GPU(N, batch);

		//Calculating European call option:
		StatsCollector stats_Euro = new StatsCollector();
		RandomVectorGenerator rvg = new AntiTheticVectorGenerator(gpu);
		EuropeanCallOption ec = new EuropeanCallOption(K_Euro);
		GBMRandomPathGenerator g = new GBMRandomPathGenerator(rate, N, vol, S0, startDate, endDate, rvg);
		for(int i = 0; i < 1000; i++){
			stats_Euro.update(ec.getPayout(g));
		}
		while(!stats_Euro.withinIntvl(err, y)){
			stats_Euro.update(ec.getPayout(g));
		}
		System.out.println("Price of European call option:" + stats_Euro.getMean()*Math.exp(-rate*(N)));
		System.out.println("Number of iteration:" + stats_Euro.getNum());

		//Calculating Asian call option:
		StatsCollector stats_Asian = new StatsCollector();
		RandomVectorGenerator rvgAsian = new AntiTheticVectorGenerator(gpu);
		AsianCallOption asian = new AsianCallOption(K_Asian);
		GBMRandomPathGenerator gAsian = new GBMRandomPathGenerator(rate, N,vol, S0, startDate, endDate, rvgAsian);
		for(int i = 0; i < 1000; i++){
			stats_Asian.update(asian.getPayout(gAsian));
		}
		while(!stats_Asian.withinIntvl(err, y)){
			stats_Asian.update(asian.getPayout(gAsian));
		}
		System.out.println("Price of Asian call option:" + stats_Asian.getMean()*Math.exp(-rate*(N)));
		System.out.println("Number of iteration:" + stats_Asian.getNum());
	}
}


