package MonteCarloSimulation;

public interface Payout {
	public double getPayout(StockPath path);
}
