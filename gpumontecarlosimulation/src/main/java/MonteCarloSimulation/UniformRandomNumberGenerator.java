package MonteCarloSimulation;

import java.util.Random;

public class UniformRandomNumberGenerator  implements RandomVectorGenerator{

    private int N;

    public UniformRandomNumberGenerator(int N){

        this.N = N;
    }

    public double[] getVector(){
        Random r = new Random();
        double[] vector = new double[N];
        for ( int i = 0; i < vector.length; ++i){
            vector[i] = r.nextDouble();
        }
        return vector;
    }
}
