package MonteCarloSimulation;

import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.joda.time.DateTime;

public class EuropeanCallOption implements Payout {

	private double K;

	public EuropeanCallOption(double K){
		this.K = K;
	}

	public double getPayout(StockPath path) {
		List<Pair<DateTime, Double>> prices = path.getPrices();
		int length = prices.size();
		return Math.max(0, prices.get(length-1).getValue() - K);
	}
}