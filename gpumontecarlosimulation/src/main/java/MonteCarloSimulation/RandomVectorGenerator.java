package MonteCarloSimulation;

/**
 * The interface for Random vector generator
 */

public interface RandomVectorGenerator {
	public double[] getVector();
}
