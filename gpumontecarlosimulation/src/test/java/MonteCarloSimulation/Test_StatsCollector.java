package MonteCarloSimulation;

/**
 * Created by tomfu on 2014/12/12.
 */
import static org.junit.Assert.*;
import junit.framework.TestCase;
import org.junit.Test;

public class Test_StatsCollector extends TestCase {
    double tolerence = 0.000001;
    double[] vector = {1,3,5,7,9};
    public void test_calculation(){
        StatsCollector stats = new StatsCollector();
        for(int i = 0; i < vector.length; i++){
            stats.update(vector[i]);
        }
        assertEquals(5.0, stats.getMean(), tolerence);
        assertEquals(33.0, stats.getMS(), tolerence);
        assertEquals(8.0, stats.getVar(), tolerence);
        System.out.println("The mean is:" + stats.getMean());
        System.out.println("The mean of square is:" + stats.getMS());
        System.out.println("The variance is:" + stats.getVar());
    }

    public void test_withinItvl(){
        double err = 0.01;
        StatsCollector stats = new StatsCollector();
        for(int i = 0; i < vector.length; i++){
            stats.update(vector[i]);
        }
        assertTrue(stats.withinIntvl(0.01, 2) == (2*Math.sqrt(8.0/5)<err));
    }

}
