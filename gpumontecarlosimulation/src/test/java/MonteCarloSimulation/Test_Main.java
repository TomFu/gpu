package MonteCarloSimulation;

/**
 * Created by tomfu on 2014/12/12.
 */
        import static org.junit.Assert.*;
        import junit.framework.TestCase;

        import org.junit.Test;
        import org.apache.commons.math3.distribution.NormalDistribution;


public class Test_Main extends TestCase{
    double tolerence = 0.000001;
    double rate = 0.0001;
    double N = 10;
    double z = 0.5;
    double[] vector = {1,2,3,4,5,6,7,8,9,10};
    public void test_NormalDistribution() {
        NormalDistribution stdNormal = new NormalDistribution();
        double y = stdNormal.inverseCumulativeProbability(z);
        assertEquals(0.0, y, tolerence);
        System.out.println("The number is:" + y);
    }

    public void test_DiscountCalculation(){
        StatsCollector stats = new StatsCollector();
        for(int i = 0; i < vector.length; i++){
            stats.update(vector[i]);
        }
        assertEquals(5.494502749, stats.getMean()*Math.exp(-rate*(N)), tolerence);
        System.out.println("The discounted mean is:" + stats.getMean()*Math.exp(-rate*(N)));
    }
}
